/*
 * Libarduinoemulator is a simple library to emulate ArduinoIDE API on a Linux PC
 * Copyright (C) 2016, 2017 Andrey V. Skvortsov <starling13@mail.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SD_HPP
#define SD_HPP

#include <Stream.h>
#include "posixcpp/posix_io_file.hpp"

#include <sys/types.h>
#include <fcntl.h>
#include <dirent.h>
#include <stdio.h>

#define FILE_READ O_RDONLY
#define FILE_WRITE (O_RDWR | O_CREAT)

#define SS 8

namespace SDCard
{
	
/**
 * @brief File open mode
 */
enum class Mode
{
	NONE = 0,
	APPEND = 0x1,
	READ = 0x2,
	WRITE = 0x4,
	CREAT = 0x8,
	READ_ONLY = 0x10
};

inline Mode operator |(Mode left, Mode right)
{
	return Mode(uint8_t(left) | uint8_t(right));
}

inline Mode operator &(Mode left, Mode right)
{
	return Mode(uint8_t(left) & uint8_t(right));
}

class File : public Stream
{
public:

	File();

	int available() override;
	void flush() override;
	int peek() override;
	int read() override;
	size_t write(uint8_t) override;

	char *name();
	bool seek(uint32_t);
	uint32_t position() const;
	uint32_t size() const;
	void close();

	operator bool();
	bool isDirectory(void);
	void rewindDirectory(void);
	File openNextFile(Mode = Mode::READ);
private:
	friend class SDClass;
	DIR *_directory;
	FILE *_file;
	std::string _path;
};

class SDClass
{
public:
	bool begin(uint8_t =0);
	File open(const char *filename, Mode = Mode::READ);
	bool remove(const char *filepath);
	bool exists(const char *filepath);
private:
	File root;
    
};

extern SDClass SDFS;

}

#endif /* SD_HPP */

