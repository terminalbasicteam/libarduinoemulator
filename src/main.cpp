/*
 * Libarduinoemulator is a simple library to emulate ArduinoIDE API on a Linux PC
 * Copyright (C) 2016-2020 Andrey V. Skvortsov <starling13@mail.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <random>
#include <csignal>

#include "emuserial.hpp"
#include "Stdiostream.hpp"

#include "Stream.h"
#include <Arduino.h>
#include <signal.h>

// Declared weak in Arduino.h to allow user redefinitions.
//int atexit(void (* /*func*/ )()) { return 0; }

// Weak empty variant initialization function.
// May be redefined by variant files.

#include "Stdiostream.hpp"
#include <pwd.h>
#include <sys/stat.h>
#include <dirent.h>
#include <unistd.h>
#include <posixcpp/posix_io_file.hpp>

StdioStream Serial;
//PseudoTtySerial	Serial(0);
#ifdef HAVE_HWSERIAL1
PseudoTtySerial	Serial1(1);
#endif
#ifdef HAVE_HWSERIAL2
PseudoTtySerial	Serial2(2);
#endif
#ifdef HAVE_HWSERIAL3
PseudoTtySerial	Serial3(3);
#endif
//StdioStream Serial(std::cin, std::cout);

void initVariant() __attribute__((weak));
void initVariant() { }

void setupUSB() __attribute__((weak));
void setupUSB() { }

void setup() __attribute__((weak));
void setup() { }

void loop() __attribute__((weak));
void loop() { }

static posix::io::File eepromFile;

uint8_t eeprom_read_byte(const uint8_t *__p)
{
	eepromFile.lseek(off_t(__p));
	posixcpp::ByteArray ba(1);
	eepromFile.read(ba);
	return ba[0];
}

void eeprom_write_byte(uint8_t *__p, uint8_t __value)
{
	eepromFile.lseek(off_t(__p));
	posixcpp::ByteArray ba(1);
	ba[0] = __value;
	eepromFile.write(ba);
}

static bool exitFlag = false;

static void sighandler(int signum)
{
	exitFlag = true;
}

static void mksd()
{
	const char *homedir = nullptr;

	if ((homedir = getenv("HOME")) == nullptr) {
		auto p = getpwuid(getuid());
		if (p != nullptr)
			homedir = p->pw_dir;
	}
	
	std::string root = std::string(homedir) + "/ucbasic/";
	
	eepromFile.setFileName(root+"eeprom.img");
	eepromFile.setAccessMode(posix::io::AccessMode_t::READ_WRITE);
	eepromFile.setCreatFlags(posix::io::CreatMode_t::CREAT);
	
	DIR *ucbasicHome = ::opendir(root.c_str());
	if (ucbasicHome == nullptr)
		::mkdir(root.c_str(), 0777);
}

int main(void) __attribute__((weak));
int main(void)
{
	//init();
	
	std::signal(SIGINT, &sighandler);
	mksd();
	
	srand(time(nullptr));
	eepromFile.open();
	
	initVariant();
	
	setup();
    
	while (!exitFlag) {
		loop();
		//if (serialEventRun) serialEventRun();
	}
	
	eepromFile.close();
        
	return EXIT_SUCCESS;
}
