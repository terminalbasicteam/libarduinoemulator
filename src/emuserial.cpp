/*
 * Terminal-BASIC is a lightweight BASIC-like language interpreter
 * Copyright (C) 2016-2019 Andrey V. Skvortsov <starling13@mail.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "emuserial.hpp"
#include <iostream>

void EmulatorSerial::begin(uint32_t baud)
{
	_tty.setBaudRate(posix::io::TTY::baudRateFromInt(baud));
}

EmulatorSerial::EmulatorSerial(posix::io::TTY	&tty) :
_tty(tty), _period(1000000000ll), _hasFirstByte(false)
{
	_tty.setAccessMode(posix::io::AccessMode_t::READ_WRITE);
	_tty.setReadTries(2);
}

int EmulatorSerial::available()
{
	return (_tty.inputQueueSize());
}

void EmulatorSerial::flush()
{
	_tty.flush(posix::io::TTY::QueueSelector_t::INOUT);
}

int EmulatorSerial::peek()
{
	posixcpp::ByteArray	ba(1);
	
	if (_hasFirstByte)
		return _firstByte;
	else {
		_tty.read(ba);
		if (ba.size() == 1) {
			_hasFirstByte = true;
			_firstByte = ba[0];
			return (_firstByte);
		} else
			return (-1);
	}
}

int EmulatorSerial::read()
{
	if (_hasFirstByte) {
		_hasFirstByte = false;
		return (_firstByte);
	} else {
		posixcpp::ByteArray	ba(1);
		_tty.read(ba);
		if (ba.size() > 0)
			return (ba[0]);
		else
			return (-1);
	}
}

size_t EmulatorSerial::readBytes(char *buffer, size_t length)
{
	posixcpp::ByteArray	ba;
	ba.wrapExistingData((uint8_t*)buffer, length);
	try {
		_tty.read(ba, posix::io::IOMode_t::LOOPED, _period);
	} catch (std::exception &ex) {
		std::cerr << ex.what() <<std::endl;
	}
	return (ba.size());
}

size_t  EmulatorSerial::write(uint8_t character)
{
	posixcpp::ByteArray	ba(1);
	ba[0] = character;
	return (_tty.write(ba));
}

PseudoTtySerial::PseudoTtySerial(uint8_t num) :
    EmulatorSerial(_tty)
{
	_tty.get();
	_tty.grant();
	_tty.unlock();
	std::cout << "Serial " << int(num) << " on pseudoterminal "
	    << _tty.slaveName() << std::endl;
}

void
PseudoTtySerial::begin(uint32_t baud)
{
	EmulatorSerial::begin(baud);
	// trick to wait for slave side open
	posix::io::TTY temp;
	temp.setFileName(_tty.slaveName());
	temp.setAccessMode(posix::io::AccessMode_t::READ_WRITE);
	temp.setCustomFlags(O_NOCTTY);
	temp.open();
	temp.close();
	posix::io::Events e(posix::io::Events_t::HANGUP), re;
	do {
		_tty.poll(e, re, 1E7);
	} while (re.testFlag(posix::io::Events_t::HANGUP));
}
