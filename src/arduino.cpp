/*
 * Terminal-BASIC is a lightweight BASIC-like language interpreter
 * Copyright (C) 2016-2019 Andrey V. Skvortsov <starling13@mail.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <unistd.h>

#include <iostream>
#include <cmath>
#include <ctime>

#include "Stream.h"
#include "Arduino.h"

#include <ctime>

char PINA, DDRA, PORTA;
char PINB, DDRB, PORTB;
char PINF, DDRF, PORTF;
char TIMSK1, TCCR1A, TCCR1B, TCCR2A, TCCR2B, OCR2A;
uint16_t ICR1, OCR1A;
int ADCSRA;

unsigned long millis()
{
    return micros() / 1000ul;
}

unsigned long micros()
{
	timespec ts;
	clock_gettime(CLOCK_MONOTONIC, &ts);
	unsigned long res = ts.tv_sec * 1E6 +
	ts.tv_nsec / 1000;
	return (res);
}

void pinMode(uint8_t number, uint8_t mode)
{
}

long random()
{
	return rand();
}

long random(long val)
{
    return random() % val;
}

void randomSeed(unsigned long val)
{
    srand(val);
}

int analogRead(uint8_t number)
{
	static double sinPhase = 0;
	double noise = (rand() % 1024) / 50.0;
	noise += (std::sin(sinPhase += 0.5)+1.0) * 512.0;
	
	return noise;
}

void analogWrite(uint8_t number, int value)
{
}

void digitalWrite(uint8_t, uint8_t)
{
}

int digitalRead(uint8_t number)
{
	return rand() % 2 ? HIGH : LOW;
}

void delayMicroseconds(unsigned int us)
{
	usleep(us);
}

void delay(unsigned long delay)
{
	delayMicroseconds(delay * 1000);
}

void
tone(uint8_t, unsigned int, unsigned long)
{
}

void noTone(uint8_t)
{
}
