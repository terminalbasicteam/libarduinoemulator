/*
 * Libarduinoemulator is a simple library to emulate ArduinoIDE API on a Linux PC
 * Copyright (C) 2016, 2017 Andrey V. Skvortsov <starling13@mail.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Stdiostream.hpp"

#include <termios.h>
#include <unistd.h>

StdioStream::StdioStream()
{
	::termios TermConf;

	::tcgetattr(STDIN_FILENO, &TermConf);
	TermConf.c_lflag &= ~(ICANON | ECHO);
	TermConf.c_cc[VMIN] = 1;
	TermConf.c_cc[VTIME] = 0;
	::tcsetattr(STDIN_FILENO, TCSANOW, &TermConf);
}

StdioStream::~StdioStream()
{
	::termios TermConf;

	::tcgetattr(STDIN_FILENO, &TermConf);
	TermConf.c_lflag |= (ICANON | ECHO);
	::tcsetattr(STDIN_FILENO, TCSANOW, &TermConf);
}

void
StdioStream::begin(uint32_t)
{
}

void
StdioStream::end()
{
}

size_t
StdioStream::write(uint8_t byte)
{
	std::cout << byte << std::flush;

	return 1;
}

static bool
inputAvailable()
{
	struct timeval tv;
	fd_set fds;
	tv.tv_sec = 0;
	tv.tv_usec = 0;
	FD_ZERO(&fds);
	FD_SET(STDIN_FILENO, &fds);
	select(STDIN_FILENO + 1, &fds, NULL, NULL, &tv);
	return FD_ISSET(0, &fds);
}

int
StdioStream::available()
{
	if (inputAvailable())
		return 1;
	else
		return 0;
}

void
StdioStream::flush()
{
	std::cout.flush();
}

int
StdioStream::peek()
{
	if (inputAvailable())
		return std::cin.peek();
	else
		return -1;
}

int
StdioStream::read()
{
	if (inputAvailable()) {
		int byte = std::cin.get();
		if (byte == '\n')
			byte = '\r';
		return byte;
	} else
		return -1;
}
