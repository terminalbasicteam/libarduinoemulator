/*
 * Libarduinoemulator is a simple library to emulate ArduinoIDE API on a Linux PC
 * Copyright (C) 2016-2018 Andrey V. Skvortsov <starling13@mail.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STDIOSTREAM_HPP
#define STDIOSTREAM_HPP

#include <iostream>
#include <Stream.h>

class StdioStream : public Stream
{
public:
	StdioStream();
	~StdioStream();
	
	void begin(uint32_t baud);
	void end();

	bool operator!() const
	{
		return (false);
	}

private:
	// Stream interface
public:
	size_t write(uint8_t) override;
	int available() override;
	int read() override;
	void flush() override;
	int peek() override;
};

#endif
