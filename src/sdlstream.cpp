/*
 * Libarduinoemulator is a simple library to emulate ArduinoIDE API on a Linux PC
 * Copyright (C) 2016, 2017 Andrey V. Skvortsov <starling13@mail.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "sdlstream.hpp"

#include <SDL.h>
#include <map>

static std::map<SDL_Keycode, char> sdl_2_ascii;

SDLStream::SDLStream() : _lastChar(-1)
{
}

void SDLStream::init()
{
	sdl_2_ascii[SDLK_0] = ')';
	sdl_2_ascii[SDLK_1] = '!';
	sdl_2_ascii[SDLK_2] = '@';
	sdl_2_ascii[SDLK_3] = '#';
	sdl_2_ascii[SDLK_4] = '$';
	sdl_2_ascii[SDLK_5] = '%';
	sdl_2_ascii[SDLK_6] = '^';
	sdl_2_ascii[SDLK_7] = '&';
	sdl_2_ascii[SDLK_8] = '*';
	sdl_2_ascii[SDLK_9] = '(';
	sdl_2_ascii[SDLK_SLASH] = '?';
	sdl_2_ascii[SDLK_RIGHTBRACKET] = '}';
	sdl_2_ascii[SDLK_LEFTBRACKET] = '{';
	sdl_2_ascii[SDLK_EQUALS] = '+';
	sdl_2_ascii[SDLK_MINUS] = '_';
	sdl_2_ascii[SDLK_BACKSLASH] = '|';
	sdl_2_ascii[SDLK_COMMA] = '<';
	sdl_2_ascii[SDLK_PERIOD] = '>';
	sdl_2_ascii[SDLK_QUOTE] = '"';
	sdl_2_ascii[SDLK_BACKQUOTE] = '~';
	sdl_2_ascii[SDLK_SEMICOLON] = ':';
}

static int
processKey(const SDL_KeyboardEvent &event)
{
	const SDL_Keysym &key = event.keysym;
	
	if (key.sym == SDLK_ESCAPE) {
		SDL_Quit();
		exit (EXIT_SUCCESS);
	} else if ((key.sym >= SDLK_a) && (key.sym <= SDLK_z)) {
		if ((key.mod & KMOD_SHIFT) != 0)
			return toupper(key.sym);
		else if (((key.mod & KMOD_CTRL) != 0) &&
		    key.sym < UINT8_MAX)
			return uint8_t(key.sym) - 'a' + 1;
		else
			return key.sym;
	} else if (key.sym < SDLK_a) {
		if (((key.mod & KMOD_SHIFT) != 0)
		    && (sdl_2_ascii.count(key.sym) > 0))
			return sdl_2_ascii.at(key.sym);
		else
			return key.sym;
	}
	
	return -1;
}

void
SDLStream::check()
{
	SDL_Event event;
	
	if (SDL_PollEvent(&event) > 0) {
		if (event.type == SDL_KEYDOWN) {
			_lastChar = processKey(event.key);
		}
	}
}

int
SDLStream::available()
{
	check();
	if (_lastChar > -1)
		return 1;
	else
		return 0;
}

void SDLStream::flush()
{

}

int SDLStream::peek()
{
	return _lastChar;
}

int SDLStream::read()
{
	check();
	const int result = _lastChar;
	_lastChar = -1;
	return result;
}

size_t SDLStream::write(uint8_t)
{
	return 0;
}
