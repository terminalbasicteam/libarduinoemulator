/*
 * Libarduinoemulator is a simple library to emulate ArduinoIDE API on a Linux PC
 * Copyright (C) 2016-2018 Andrey V. Skvortsov <starling13@mail.ru>
 * Copyright (C) 2019,2020 Terminal-BASIC team
 *     <https://bitbucket.org/%7Bf50d6fee-8627-4ce4-848d-829168eedae5%7D/>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SDLSTREAM_HPP
#define SDLSTREAM_HPP

#include "Stream.h"

/**
 * @brief SDL2 input stream
 */
class SDLStream : public Stream
{
public:
    
    SDLStream();
    
    /**
     * @brief should be called before usage
     */
    void init();
    
    int available() override;
    
    void flush() override;

    int peek() override;
    /**
     * @breif dummy
     */
    size_t write(uint8_t) override;
    
    int read() override;

private:
    
    void check();
    
    int _lastChar;
};

#endif // SDLSTREAM_HPP
