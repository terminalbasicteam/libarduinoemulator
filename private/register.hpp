/*
 * Libarduinoemulator is a simple library to emulate ArduinoIDE API on a Linux PC
 * Copyright (C) 2016, 2017 Andrey V. Skvortsov <starling13@mail.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef REGISTER_HPP
#define REGISTER_HPP

#include <stdint.h>

template <typename T>
class Register
{
public:
	enum Size
	{
		U8 = 8,
		U16 = 16,
		U32 = 32
	};
	
	explicit Register() = default;
	~Register() = default;
	
	Register &operator =(T newVal)
	{
		this->writeEvet(newVal);
		
		return *this;
	}
	
	operator T()
	{
		return this->readEVent();
	}
	
	virtual Size size() const = 0;
	
protected:
	
	virtual void writeEvet(T newVal) = 0;
	virtual T readEVent() = 0;
	
private:

	Register(const Register&) = delete;
	Register(Register&&) = delete;
	Register &operator =(const Register&) = delete;
};

class RegisterU8 : public Register<uint8_t>
{
public:
	RegisterU8() = default;
	virtual ~RegisterU8() = default;
	Register<uint8_t>::Size size() const override { return U8; }
private:
	RegisterU8(const RegisterU8&) = delete;
	RegisterU8(RegisterU8&&) = delete;
	RegisterU8 &operator =(const RegisterU8&) = delete;
};

typedef Register<uint16_t> RegisterU16;
typedef Register<uint32_t> RegisterU32;

#endif
