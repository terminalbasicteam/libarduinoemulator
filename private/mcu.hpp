/*
 * Libarduinoemulator is a simple library to emulate ArduinoIDE API on a Linux PC
 * Copyright (C) 2016, 2017 Andrey V. Skvortsov <starling13@mail.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MCU_HPP
#define MCU_HPP

#include "register.hpp"

class MCU
{
};

class RegisterU8_RO : RegisterU8
{
public:
	RegisterU8_RO(uint8_t &val) : value(val) {}

protected:
	uint8_t readEVent() override
	{
		return value;
	}
	void writeEvet(uint8_t) override {}
private:
	uint8_t &value;
};

class PORT
{
public:
	
	PORT(uint8_t &val) : value(val) {}
private:
	
	uint8_t &value;
};

class ATmega328MCU : public MCU
{
public:
	ATmega328MCU();
	
	static ATmega328MCU _instance;
private:
	uint8_t _B, _C, _D;
public:
	PORT portB, portC, portD;
};

#endif
