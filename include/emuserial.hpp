/*
 * Libarduinoemulator is a simple library to emulate ArduinoIDE API on a Linux PC
 * Copyright (C) 2016-2018 Andrey V. Skvortsov <starling13@mail.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * @fileemuserial.hpp
 */

#ifndef EMUSERIAL_HPP
#define EMUSERIAL_HPP

#include "posix_io_pseudotty.hpp"
#include <Stream.h>

class EmulatorSerial : public Stream
{
public:
	EmulatorSerial(posix::io::TTY&);

	virtual void begin(uint32_t baud);
	size_t readBytes(char*, size_t) override;

	size_t write(uint8_t) override;

	posix::io::TTY &tty()
	{
		return (_tty);
	}
	
	bool operator!() const
	{
		return false;
	}
	
private:
	posix::io::TTY &_tty;
	uint64_t _period;
	uint8_t	_firstByte;
	bool _hasFirstByte;
	
	// Stream interface
public:
	int available() override;
	int read() override;
	void flush() override;;
	int peek() override;;
};

class PseudoTtySerial : public EmulatorSerial
{
public:
	PseudoTtySerial(uint8_t);
	void begin(uint32_t) override;
	
	using Print::write;
private:
	posix::io::PseudoTTY _tty;
};

#endif /* EMUSERIAL_HPP */

