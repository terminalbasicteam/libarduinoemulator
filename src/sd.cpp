/*
 * Libarduinoemulator is a simple library to emulate ArduinoIDE API on a Linux PC
 * Copyright (C) 2016-2018 Andrey V. Skvortsov <starling13@mail.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "sd.hpp"
#include <libgen.h>
#include <iostream>
#include <pwd.h>
#include <sys/stat.h>
#include <unistd.h>

namespace SDCard
{

SDClass SDFS;

File::File() :
_directory(nullptr),
_file(nullptr)
{
}

bool
SDClass::begin(uint8_t csPin)
{	
	const char *homedir;

	if ((homedir = getenv("HOME")) == nullptr) {
		auto p = getpwuid(getuid());
		if (p == nullptr)
			return false;
		homedir = p->pw_dir;
	}
	root._path = homedir;
	root._path += "/ucbasic/SD/";
	root._directory = ::opendir(root._path.c_str());
	if (root._directory == nullptr) {
		::mkdir(root._path.c_str(), 0777);
		root._directory = ::opendir(root._path.c_str());
		if (root._directory == nullptr)
			return false;
	}
	
	return true;
}

File
File::openNextFile(Mode)
{
	File result;
	if (this->_directory == nullptr)
		return result;
	
	dirent *e;
next:
	e = readdir(_directory);
	if (e != nullptr) {
		if (e->d_name[0] == '.')
			goto next;
		std::string path = _path + '/' + e->d_name;
		if (e->d_type == DT_REG) {
			result._file = ::fopen(path.c_str(), "r+");
			if (result._file == nullptr)
				::perror("fopen");
		} else if (e->d_type == DT_DIR)
			result._directory = ::opendir(path.c_str());
		result._path = path;
	}
	
	return result;
}

char*
File::name()
{
	return ::basename((char*)_path.c_str());
}

bool
File::seek(uint32_t position)
{
	if (_file != nullptr) {
		if (::fseek(_file, 0l, SEEK_SET) == 0)
			return true;
	}
	return false;
}

uint32_t
File::position() const
{
	uint32_t result = 0;
	
	if (_file != nullptr)
		result = ::ftell(_file);
	
	return result;
}

uint32_t
File::size() const
{
	uint32_t result = 0;
	
	if (_file != nullptr) {
		uint32_t tmp = ::ftell(_file);
		if (::fseek(_file, 0, SEEK_END) == 0) {
			result = ::ftell(_file);
			::fseek(_file, tmp, SEEK_SET);
		}
	}
	
	return result;
}

File
SDClass::open(const char* filename, Mode)
{
	File result;

	std::string path = root._path + filename;
	if (std::string(filename) == "/") {
		result._directory = ::opendir(root._path.c_str());
		result._path = root._path;
		return result;
	}

	std::string pathTmp(path.c_str());
	std::string base = ::basename((char*)pathTmp.c_str()); 
	
	pathTmp = path.c_str();
	char *dir = ::dirname((char*)pathTmp.c_str());
	DIR *d = ::opendir(dir);
	if (d != nullptr) {
		dirent * e;
		while ((e = ::readdir(d)) != nullptr) {
			if (e->d_name[0] == '.')
				continue;
			if (std::string(e->d_name) == base) {
				if (e->d_type == DT_REG)
					result._file = ::fopen(path.c_str(), "r+");
				else if (e->d_type == DT_DIR)
					result._directory = ::opendir(path.c_str());
				result._path = path;
				break;
			}
		}
	} else
		throw std::runtime_error(std::string("Can't open directory").
		    append(dir));
	::closedir(d);
	
	if (!result)
		result._file = ::fopen(path.c_str(), "w+");
	
	return result;
}

int
File::available()
{
	int result = 0;
	if (_file != nullptr) {
		uint32_t current = ::ftell(_file);
		uint32_t end = this->size();
		result = end - current;
	}
	return result;
}

void
File::close()
{
	if (_file != nullptr) {
		::fclose(_file);
		_file = nullptr;
	} else if (_directory != nullptr) {
		::closedir(_directory);
		_directory = nullptr;
	}
}

void
File::flush()
{
	if (_file != nullptr)
		::fflush(_file);
}

File::operator bool()
{
	return (_file != nullptr) || (_directory != nullptr);
}

int
File::peek()
{
	int result = -1;
	if (_file != nullptr) {
		result = ::fgetc(_file);
		::ungetc(result, _file);
	}
	return result;
}

int
File::read()
{
	if (_file)
		return ::fgetc(_file);
	else
		return -1;
}

size_t
File::write(uint8_t c)
{
	if (_file) {
		::fputc(c, _file);
		return 1;
	} else
		return 0;
}

bool
File::isDirectory()
{
	return (_file == nullptr) || (_directory != nullptr);
}

bool
SDClass::exists(const char* filepath)
{
	std::string path = root._path+filepath;
	FILE *temp;
	if ((temp = fopen(path.c_str(), "r")) != nullptr) {
		fclose(temp);
		return true;
	} else
		return false;
}

bool
SDClass::remove(const char* filepath)
{
	return ::unlink((root._path+filepath).c_str()) == 0;
}

void
File::rewindDirectory()
{
	if (_directory != nullptr)
		::rewinddir(_directory);
}

} // namespace SDCard
