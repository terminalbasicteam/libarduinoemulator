/*
 * Libarduinoemulator is a simple library to emulate ArduinoIDE API on a Linux PC
 * Copyright (C) 2016-2018 Andrey V. Skvortsov <starling13@mail.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STREAM_H
#define STREAM_H

#include "Print.h"

class Stream : public Print
{
public:
	Stream() {_timeout=1000;}
	
	size_t readBytesUntil( char terminator, char *buffer, size_t length); // as readBytes with terminator character
	size_t readBytesUntil( char terminator, uint8_t *buffer, size_t length) { return readBytesUntil(terminator, (char *)buffer, length); }
	
	void setTimeout(unsigned long timeout) { _timeout = timeout; }  // sets maximum milliseconds to wait for stream data, default is 1 second
	unsigned long getTimeout(void) { return _timeout; }
	
	int timedRead();    // private method to read stream with timeout
	virtual int available() = 0;
	virtual int read() = 0;
	virtual int peek() = 0;
	virtual void flush() = 0;
	virtual size_t readBytes( char *buffer, size_t length); // read chars from stream into buffer
	size_t readBytes( uint8_t *buffer, size_t length) { return readBytes((char *)buffer, length); }
	
	bool findUntil(char *target, char *terminator);   // as find but search ends if the terminator string is found
	bool findUntil(char *target, size_t targetLen, char *terminate, size_t termLen);   // as above but search ends if the terminate string is found
	struct MultiTarget {
		const char *str;  // string you're searching for
		size_t len;       // length of string you're searching for
		size_t index;     // index used by the search routine.
	};
    
	// This allows you to search for an arbitrary number of strings.
	// Returns index of the target that is found first or -1 if timeout occurs.
	int findMulti(struct MultiTarget *targets, int tCount);
private:
	unsigned long _timeout;
	unsigned long _startMillis;  // used for timeout measurement
};

#endif
